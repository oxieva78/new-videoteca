<?php

declare(strict_types=1);

namespace App\Users\User\Application\SignUpUser;

use App\Shared\Domain\Exceptions\DomainException;
use App\Shared\Infrastructure\Services\UniqueIdProviderInterface;
use App\Users\User\Application\Exceptions\UserInvalidException;
use App\Users\User\Application\Service\UserCreator;
use App\Users\User\Domain\Exceptions\PasswordInvalidByPolicyRulesException;
use App\Users\User\Domain\Exceptions\UserNameInvalidByPolicyRulesException;
use App\Users\User\Domain\ValueObjects\Email;
use App\Users\User\Domain\ValueObjects\Password;
use App\Users\User\Domain\ValueObjects\UserId;
use App\Users\User\Domain\ValueObjects\UserName;
use App\Users\User\Ui\Http\Api\Rest\SignUpUserPresenter;
use Exception;
use ReflectionException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class SignUpUserCommandHandler implements MessageHandlerInterface
{
    private UniqueIdProviderInterface $uniqueUuidProviderService;
    private UserCreator $userBuilder;
    private SignUpUserPresenter $signUpUserPresenter;

    public function __construct(
        UniqueIdProviderInterface $uniqueUuidProviderService,
        UserCreator $userBuilder,
        SignUpUserPresenter $signUpUserPresenter
    ) {
        $this->uniqueUuidProviderService = $uniqueUuidProviderService;
        $this->userBuilder = $userBuilder;
        $this->signUpUserPresenter = $signUpUserPresenter;
    }

    /**
     * @throws DomainException
     * @throws PasswordInvalidByPolicyRulesException
     * @throws UserInvalidException
     * @throws UserNameInvalidByPolicyRulesException
     * @throws ReflectionException
     * @throws Exception
     */
    public function __invoke(SignUpUserCommand $command): SignUpUserPresenter
    {
        $userId = $this->uniqueUuidProviderService->generate();

        $user = $this->userBuilder->create(
            UserId::fromString($userId),
            UserName::build($command->username()),
            Email::build($command->email()),
            Password::build($command->password())
        );

        $this->signUpUserPresenter->write($user);

        return $this->signUpUserPresenter;
    }
}
