<?php declare(strict_types=1);

namespace App\Users\User\Ui\Http\Api\Rest;

use App\Users\User\Domain\User;

final class SignUpUserPresenter
{
    private User $user;

    public function write(User $user)
    {
        $this->user = $user;
    }

    public function read(): array
    {
        return [
            'id' => $this->user->userId()->value(),
            'userName' => $this->user->username()->value(),
            'email' => $this->user->email()->value(),
            'password' => $this->user->password()->value(),
        ];
    }
}
