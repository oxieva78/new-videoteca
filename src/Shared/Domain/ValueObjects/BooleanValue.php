<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObjects;

class BooleanValue
{
    private bool $value;

    final private function __construct(bool $value)
    {
        $this->value = $value;
    }

    final public static function build(bool $value)
    {
        return new static($value);
    }

    final public function value(): bool
    {
        return $this->value;
    }

    final public function equals(self $valueObject): bool
    {
        return $this->value === $valueObject->value();
    }
}
