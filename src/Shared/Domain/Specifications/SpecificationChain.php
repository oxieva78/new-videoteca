<?php

declare(strict_types=1);

namespace App\Shared\Domain\Specifications;

use ReflectionClass;
use ReflectionException;

abstract class SpecificationChain implements SpecificationChainInterface
{
    protected array $specifications;
    protected SpecificationChainResultCollection $specificationChainResultCollection;

    public function __construct()
    {
        $this->specificationChainResultCollection = SpecificationChainResultCollection::build();
    }

    /**
     * @throws ReflectionException
     */
    final protected function processSpecificationResult(bool $isSatisfied, SpecificationInterface $specification)
    {
        $message = '';

        if (!$isSatisfied) {
            $message = $specification->getFailedMessage();
        }

        $this->specificationChainResultCollection->addResult(SpecificationChainResult::build(
            (new ReflectionClass($specification))->getShortName(),
            $isSatisfied,
            $message
        ));
    }

    final public function getResultCollection(): SpecificationChainResultCollection
    {
        return $this->specificationChainResultCollection;
    }

    /**
     * @return SpecificationChainResult[]
     */
    final public function getFailedResults(): array
    {
        return $this->specificationChainResultCollection->getFailedResults();
    }

    final public function getFailedResultsAsString(): string
    {
        return $this->specificationChainResultCollection->getFailedResultsAsString();
    }

    final protected function returnFalseIfNoSpecifications(): bool
    {
        return count($this->specifications) > 0;
    }

    final protected function updateResult(bool $result, bool $isSatisfied): bool
    {
        if (!$isSatisfied || !$result) {
            return false;
        }

        return true;
    }
}
