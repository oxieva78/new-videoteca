<?php declare(strict_types=1);

namespace App\Shared\Domain\Specifications;

class SpecificationChainResult
{
    private string $specification;
    private bool $value;
    private string $message;

    private function __construct(string $specification, bool $value, string $message)
    {
        $this->specification = $specification;
        $this->value = $value;
        $this->message = $message;
    }

    public static function build(string $specification, bool $value, string $message): self
    {
        return new static($specification, $value, $message);
    }

    public function specification(): string
    {
        return $this->specification;
    }

    public function value(): bool
    {
        return $this->value;
    }

    public function message(): string
    {
        return $this->message;
    }
}
