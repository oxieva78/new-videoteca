<?php

declare(strict_types=1);

namespace App\Shared\Domain\Specifications;

use Doctrine\Common\Collections\ArrayCollection;

class SpecificationChainResultCollection extends ArrayCollection
{
    private function __construct()
    {
        parent::__construct([]);
    }

    public static function build(): self
    {
        return new static();
    }

    /** @return SpecificationChainResult[] */
    public function getResults(): array
    {
        return $this->toArray();
    }

    public function getFailedResults(): array
    {
        /** @var SpecificationChainResult[] $results */
        $results = $this->toArray();
        $failedResults = [];

        foreach ($results as $specificationResult) {
            if (!$specificationResult->value()) {
                $failedResults[] = $specificationResult;
            }
        }

        return $failedResults;
    }

    public function getFailedResultsAsString(): string
    {
        /** @var SpecificationChainResult[] $results */
        $results = $this->toArray();
        $failedResults = '';

        foreach ($results as $specificationResult) {
            if (!$specificationResult->value()) {
                $failedResults = $failedResults . $specificationResult->message(). ', ';
            }
        }

        return substr($failedResults, 0, strlen($failedResults) - 2) ;
    }

    public function addResult(SpecificationChainResult $specificationChainResult)
    {
        $this->add($specificationChainResult);
    }
}
