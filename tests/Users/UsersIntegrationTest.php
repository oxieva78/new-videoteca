<?php declare(strict_types=1);

namespace App\Tests\Users;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UsersIntegrationTest extends WebTestCase
{
    public function testUserCanSignUp()
    {
        $client = self::createClient();
        $client->request('POST', '/users', [], [], [], json_encode([
            'userName' => 'JohnDoe',
            'email' =>  'test.email@gmail.com',
            'password' => ',\u0026+3RjwAu88(tyC\u0027'
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
