<?php

declare(strict_types=1);

namespace App\Tests\Users\User\Domain;

use App\Tests\Users\User\Domain\TestMothers\UserMother;
use App\Users\User\Domain\User;
use Error;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testUserCannotBeInstantiatedDirectly()
    {
        self::expectException(Error::class);

        new User();
    }

    public function testUserCanBeBuilt()
    {
        $user = UserMother::forTest();

        self::assertInstanceOf(User::class, $user);
        self::assertEquals(UserMother::USER_UUID, $user->userId()->value());
        self::assertEquals(UserMother::USERNAME, $user->username()->value());
        self::assertEquals(UserMother::EMAIL, $user->email()->value());
        self::assertEquals(UserMother::PASSWORD, $user->password()->value());
    }
}
