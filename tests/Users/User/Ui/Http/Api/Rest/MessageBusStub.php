<?php declare(strict_types=1);

namespace App\Tests\Users\User\Ui\Http\Api\Rest;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Handler\HandlerDescriptor;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class MessageBusStub implements MessageBusInterface
{
    private $handler;

    public function __construct($handler = null)
    {
        $this->handler = $handler;
    }

    public function dispatch($message, array $stamps = []): Envelope
    {
        $result = $this->handler->__invoke($message);

        $handledStamp = HandledStamp::fromDescriptor(new HandlerDescriptor($this->handler), $result);
        return Envelope::wrap($result, [$handledStamp]);
    }
}
