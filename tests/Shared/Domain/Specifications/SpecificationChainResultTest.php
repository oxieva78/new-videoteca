<?php declare(strict_types=1);

namespace App\Tests\Shared\Domain\Specifications;

use App\Shared\Domain\Specifications\SpecificationChainResult;
use Error;
use PHPUnit\Framework\TestCase;

class SpecificationChainResultTest extends TestCase
{
    public function testCannotBeInstantiatedDirectly()
    {
        self::expectException(Error::class);

        new SpecificationChainResult();
    }

    public function testCanBeBuilt()
    {
        $specificationChainResult = SpecificationChainResult::build('specification', true, 'message');
        self::assertEquals('specification', $specificationChainResult->specification());
        self::assertEquals(true, $specificationChainResult->value());
        self::assertEquals('message', $specificationChainResult->message());
    }
}
