<?php

declare(strict_types=1);

namespace App\Tests\Shared\Domain\ValueObjects;

use App\Shared\Domain\ValueObjects\BooleanValue;
use Error;
use PHPUnit\Framework\TestCase;

class BooleanForTest extends BooleanValue
{
}

class BooleanValueTest extends TestCase
{
    public function testBooleanValueCannotBeInstantiatedDirectly()
    {
        self::expectException(Error::class);

        new BooleanForTest();
    }

    public function testBooleanValueIsAccessible()
    {
        $boolean = BooleanForTest::build(true);

        self::assertEquals(true, $boolean->value());
    }

    public function testEqualsFunction()
    {
        $boolean = BooleanForTest::build(true);

        self::assertTrue($boolean->equals(BooleanForTest::build(true)));
        self::assertFalse($boolean->equals(BooleanForTest::build(false)));
    }
}
